import 'package:firebase_auth_live/app/controllers/auth_controller.dart';
import 'package:firebase_auth_live/app/global_widgets/widgets.dart';
import 'package:firebase_auth_live/app/utils/logger.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/sign_in_controller.dart';

class SignInView extends GetView<SignInController> {
  SignInView({Key? key}) : super(key: key);
  final authController = Get.find<AuthController>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('SignInView'),
        centerTitle: true,
      ),
      body: Container(
        margin: const EdgeInsets.all(25),
        child: Obx(() => authController.isLoading.value
            ? LoadingScreen()
            : Column(
                children: [
                  InputField(
                    prefixIcon: Icon(Icons.email),
                    textEditingController: controller.emailController,
                    hintText: 'Masukkan Email Anda',
                    onChanged: controller.emailChanged,
                    errorMsg: controller.emailErrorText.value,
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  InputField(
                    obscureText: true,
                    prefixIcon: Icon(Icons.lock),
                    textEditingController: controller.passwordController,
                    hintText: 'Masukkan Kata Sandi Anda',
                    onChanged: controller.passwordChanged,
                    errorMsg: controller.passwordErrorText.value,
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  SimpleButton(
                    label: 'Sign In',
                    onPressed: () => authController.signIn(
                        controller.emailController.text,
                        controller.passwordController.text),
                  )
                ],
              )),
      ),
    );
  }
}
