part of 'widgets.dart';

class SimpleButton extends StatelessWidget {
  const SimpleButton({super.key, this.onPressed, required this.label, this.color = Colors.blue});

  final VoidCallback? onPressed;
  final String label;
  final Color color;

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(onPressed: onPressed, child: Text(label), style: ElevatedButton.styleFrom(
      backgroundColor: color, minimumSize: Size.fromHeight(Get.height * 0.05)
    ),);
  }
}