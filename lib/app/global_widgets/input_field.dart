part of 'widgets.dart';

class InputField extends StatelessWidget {
  const InputField({super.key, this.prefixIcon, this.obscureText = false, 
   required this.textEditingController, this.errorMsg, 
  required this.hintText, this.color = Colors.blue, this.suffixIcon, required this.onChanged});
  final TextEditingController textEditingController;
  final Function(String) onChanged;
  final String? errorMsg;
  final String hintText;
  final Color color;
  final Widget? prefixIcon;
  final Widget? suffixIcon;
  final bool obscureText;

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      obscureText: obscureText,
      onChanged: onChanged,
      controller: textEditingController,
      cursorColor: Colors.black12,
      decoration: InputDecoration(
        prefixIcon: prefixIcon,
        suffixIcon: suffixIcon,
        errorText: errorMsg,
        hintText: hintText,
        filled: true,
        fillColor: Colors.white,
        focusColor: Colors.blue,
        hoverColor: Colors.blue,
        errorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(12),
          borderSide: BorderSide(color: Colors.red),
        )
      ),
    );
  }
}