import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_auth_live/app/routes/app_pages.dart';
import 'package:firebase_auth_live/app/utils/logger.dart';
import 'package:get/get.dart';

class AuthController extends GetxController {
  FirebaseAuth auth = FirebaseAuth.instance;
  RxBool isLoading = false.obs;

  signUp(String email, String password) async {
    isLoading(true);
    try {
      final credential = await auth.createUserWithEmailAndPassword(
        email: email,
        password: password,
      );
      logger.d(credential);
      Get.offAllNamed(Routes.HOME);
    } on FirebaseAuthException catch (e) {
      if (e.code == 'weak-password') {
        logger.d('The password provided is too weak.');
      } else if (e.code == 'email-already-in-use') {
        logger.d('The account already exists for that email.');
      }
    } catch (e) {
      logger.d(e);
    } finally {
      //finally ini gunanya, kalau ada error atau sukses, maka
      //isloading ini akan tetap kepanggil
      isLoading(false);
    }
  }

  signIn(String email, String password) async {
    isLoading(true);
    try {
      final credential = await auth.signInWithEmailAndPassword(
        email: email,
        password: password,
      );
      logger.d(credential);
      Get.offAllNamed(Routes.HOME);
    } on FirebaseAuthException catch (e) {
      if (e.code == 'weak-password') {
        logger.d('The password provided is too weak.');
      } else if (e.code == 'email-already-in-use') {
        logger.d('The account already exists for that email.');
      }
    } catch (e) {
      logger.d(e);
    } finally {
      //finally ini gunanya, kalau ada error atau sukses, maka
      //isloading ini akan tetap kepanggil
      isLoading(false);
    }
  }

  signInWithGoogle() async {}
  signOut() async {
    isLoading(true);
    try {
      await auth.signOut();
      Get.offAllNamed(Routes.SIGN_IN);
      logger.d('SIGN OUT');
    } catch (e) {
      logger.d(e);
    } finally {
      isLoading(false);
    }
  }
}
